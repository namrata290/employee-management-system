from django.db import models
from django.contrib.auth.models import User

class Question(models.Model):
    title = models.TextField(null=True, blank="True")
    status = models.CharField(default="inactive", max_length=10)
    created_by = models.ForeignKey(User,null=True, blank="True", on_delete="CASCADE")
    
    created_at = models.DateTimeField(auto_now_add=True)  # only once
    updated_at = models.DateTimeField(auto_now=True)    # every time
    

    def __str__(self):
        return self.title


